package ru.nsu.patrushev.lab2.dao;
import ru.nsu.patrushev.lab2.model.osm.Node;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface NodeDao {

    boolean insertNodeUsingPreparedStatement(Node node);

    boolean insertNodeUsingStatement(Node node);

    void insertBatch(List<Node> nodes);

    Optional<Node> getNodeById(Integer id);

    boolean updateNode(Node node);

    boolean deleteNode(Integer id);


    default Node extractNodeFromResultSet(ResultSet rs) throws Exception {
        Node node = new Node();
        node.setId(rs.getInt("id"));
        node.setVersion(rs.getInt("version"));
        XMLGregorianCalendar xmlGregorianCalendar = convertLocalDataTimeToXmlGCal(rs.getTimestamp("timestamp"));
        node.setTimestamp(xmlGregorianCalendar);
        node.setUid(rs.getInt("uid"));
        node.setUser(rs.getString("user"));
        node.setChangeset(rs.getInt("changeset"));
        node.setLat(rs.getDouble("lat"));
        node.setLon(rs.getDouble("lon"));
        return node;
    }

    default XMLGregorianCalendar convertLocalDataTimeToXmlGCal(Timestamp timestamp) throws Exception {
        LocalDateTime localDateTime = timestamp.toLocalDateTime();
        XMLGregorianCalendar cal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        cal.setYear(localDateTime.getYear());
        cal.setMonth(localDateTime.getMonthValue());
        cal.setDay(localDateTime.getDayOfMonth());
        cal.setHour(localDateTime.getHour());
        cal.setMinute(localDateTime.getMinute());
        cal.setSecond(localDateTime.getSecond());
        cal.setFractionalSecond(new BigDecimal("0." + localDateTime.getNano()));
        return cal;
    }
}
