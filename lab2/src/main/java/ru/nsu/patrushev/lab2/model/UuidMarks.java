package ru.nsu.patrushev.lab2.model;

import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor
public class UuidMarks {
    int uuid;

    long amountOfMarks;

}
