package ru.nsu.patrushev.lab2;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.xml.bind.JAXBException;

@Data
public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class.getName());
    private final NodeLoader nodeLoader;

    public Main() throws JAXBException {
        nodeLoader = new NodeLoader();
    }

    public static void main(String[] args) throws JAXBException {
        Main main = new Main();
        main.nodeLoader.loadNodesWithDifferentWays();
    }
}
