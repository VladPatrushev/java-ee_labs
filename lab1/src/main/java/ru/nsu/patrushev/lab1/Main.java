package ru.nsu.patrushev.lab1;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import lombok.Data;

@Data
public class Main {
    private OpenStreetMapStatsProcessing openStreetMapStatsProcessing;
    private static final Logger logger = LogManager.getLogger(Main.class.getName());

    public Main() {
        openStreetMapStatsProcessing = new OpenStreetMapStatsProcessing();
    }

    public static void main(String[] args) {
        Main mainProcessing = new Main();
        mainProcessing.openStreetMapStatsProcessing.processStats();
    }
}
