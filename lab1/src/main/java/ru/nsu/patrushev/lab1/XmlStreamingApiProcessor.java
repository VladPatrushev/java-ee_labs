package ru.nsu.patrushev.lab1;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import static java.lang.String.format;


public class XmlStreamingApiProcessor implements AutoCloseable {
    private static final Logger logger = LogManager.getLogger(XmlStreamingApiProcessor.class.getName());
    private static final XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    private final XMLStreamReader reader;

    public XmlStreamingApiProcessor(InputStream is) throws XMLStreamException {
        reader = xmlInputFactory.createXMLStreamReader(is);
    }

    public XMLStreamReader getReader() {
        return reader;
    }

    @Override
    public void close() {
        if (reader != null) {
            try {
                reader.close();
            } catch (XMLStreamException e) {
                logger.error(format("ERROR - %s", e.getLocalizedMessage()));
            }
        }
    }
}
