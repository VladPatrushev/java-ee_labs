package ru.nsu.patrushev.lab1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.patrushev.lab1.model.UUidMarks;
import ru.nsu.patrushev.lab1.model.UserChanges;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;

public class OpenStreetMapStatsProcessing {
    private static final Logger logger = LogManager.getLogger(OpenStreetMapStatsProcessing.class.getName());
    private Map<String, Long> userChangesMap = new HashMap<>();
    private Map<String, Long> userIdMarksMap = new HashMap<>();
    private List<UserChanges> sortedUserChangesList;
    private List<UUidMarks> sortedUUidMarksList;
    private long amountOfNodes = 0;


    public void processStats() {
        URL resource = getClass().getClassLoader().getResource("RU-NVS.osm");
        try (XmlStreamingApiProcessor processor = new XmlStreamingApiProcessor(Files.newInputStream(Path.of(Objects.requireNonNull(resource).getPath())))) {
            XMLStreamReader reader = processor.getReader();
            while (reader.hasNext()) {
                int event = reader.next();
                if (event == XMLEvent.START_ELEMENT && "node".equals(reader.getLocalName())) {
                    amountOfNodes++;
                    int userNameIndex = -1;
                    int userIdIndex = -1;
                    for (int i = 0; i < reader.getAttributeCount(); i++) {
                        final String attributeName = reader.getAttributeLocalName(i);
                        if ("user".equals(attributeName)) {
                            userNameIndex = i;
                        }

                        if ("uid".equals(attributeName)) {
                            userIdIndex = i;
                        }
                    }

                    String user = reader.getAttributeValue(userNameIndex);
                    userChangesMap.compute(user, (k, v) -> (v == null) ? 1 : 1 + v);
                    String uuid = reader.getAttributeValue(userIdIndex);
                    userIdMarksMap.compute(uuid, (k, v) -> (v == null) ? 1 : 1 + v);
                }
            }
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
            logger.error("Processing Stats");

        }
        logger.info("Processing Stats");
        sortedUserChangesList = userChangesMap.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .map((entry) -> new UserChanges(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

        sortedUUidMarksList = userIdMarksMap.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .map((entry) -> new UUidMarks(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

        logger.info(format("Size of list is %s", sortedUserChangesList.size()));
        logger.info(format("Amount of nodes is %s", amountOfNodes));

        StringBuilder userChangesOutput = new StringBuilder("first\n");
        sortedUserChangesList.stream().limit(30).forEach(userChanges -> {
            logger.info((format("User %s amount of changes is %s", userChanges.getUser(), userChanges.getAmountOfChanges())));
            userChangesOutput
                    .append(format("User %s amount of changes is %s", userChanges.getUser(), userChanges.getAmountOfChanges()))
                    .append("\n");

        });

        StringBuilder uuidMarksOutput = new StringBuilder("second\n");
        sortedUUidMarksList.stream().limit(30).forEach(uuidMarks -> {
            logger.info((format("UUID %s amount of marks is %s", uuidMarks.getUuid(), uuidMarks.getAmountOfMarks())));
            uuidMarksOutput
                    .append((format("UUID %s amount of marks is %s", uuidMarks.getUuid(), uuidMarks.getAmountOfMarks())))
                    .append("\n");
        });

        System.out.println(userChangesOutput);
        System.out.println(uuidMarksOutput);
    }
}
