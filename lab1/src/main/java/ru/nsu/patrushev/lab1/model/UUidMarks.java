package ru.nsu.patrushev.lab1.model;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor
public class UUidMarks {
    String uuid;
    long amountOfMarks;
}
